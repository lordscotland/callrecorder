ARCHS = arm64

include theos/makefiles/common.mk

TWEAK_NAME = CallRecorder
CallRecorder_FILES = Tweak.m
CallRecorder_FRAMEWORKS = AudioToolbox CoreFoundation
CallRecorder_PRIVATE_FRAMEWORKS = VoiceMemos

TWEAK_NAME += CallRecorderUI
CallRecorderUI_FILES = UI.x
CallRecorderUI_FRAMEWORKS = UIKit CoreGraphics

include $(THEOS_MAKE_PATH)/tweak.mk
