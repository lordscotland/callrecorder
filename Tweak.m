#import <AudioToolbox/AudioToolbox.h>
#import <CoreFoundation/CoreFoundation.h>
#include <substrate.h>
#include "common.h"

static CFMutableDictionaryRef _StreamMap;

typedef struct {
  void* __vtable;
  UInt32 typeID;
  char* name;
  AudioStreamBasicDescription format;
} DSPRouting_Stream;
struct stream_info {
  char name[8];
  AudioBufferList* peerData;
  ExtAudioFileRef outputFile;
  CFURLRef outputURL;
  CFAbsoluteTime absoluteTime;
  Float64 duration;
  int token[2];
  bool tokenValid[2];
};

@interface RCSavedRecordingsModel
+(NSString*)savedRecordingsDirectory;
+(instancetype)sharedModel;
-(id)insertRecordingWithAudioFile:(NSString*)path duration:(Float64)duration date:(NSDate*)date customLabelBase:(NSString*)label;
@end

static void (*F_DSPRoutingTelephony_Initialize)(void*);
static void f_DSPRoutingTelephony_Initialize(void* self){
  F_DSPRoutingTelephony_Initialize(self);
  struct stream_info* info=(void*)CFDictionaryGetValue(_StreamMap,self);
  if(!info){
    info=malloc(sizeof(*info)*2);
    info->name[0]='*';
    info->name[1]='B';
    info->name[2]='B';
    info->name[3]='_';
    info->name[4]='u';
    info->name[5]='p';
    info->name[6]=info->name[7]=0;
    info->outputFile=NULL;
    info->tokenValid[0]=(notify_register_dispatch(CALLRECORDER_DISABLE,&info->token[0],
     dispatch_get_main_queue(),^(int token){info->name[0]='-';})==NOTIFY_STATUS_OK);
    info->tokenValid[1]=(notify_register_dispatch(CALLRECORDER_ENABLE,&info->token[1],
     dispatch_get_main_queue(),^(int token){info->name[0]='+';})==NOTIFY_STATUS_OK);
    
    CFDictionaryAddValue(_StreamMap,self,info);
  }
  (*(DSPRouting_Stream**)((char*)self+288))->name=info->name;
  info->peerData=(void*)(*(char**)(*(char**)((char*)self+296)+72)+24);
}
static void (*F_DSPRoutingTelephony_destroy)(void*);
static void f_DSPRoutingTelephony_destroy(void* self){
  struct stream_info* info=(void*)CFDictionaryGetValue(_StreamMap,self);
  CFDictionaryRemoveValue(_StreamMap,self);
  if(info){
    if(info->outputFile){
      ExtAudioFileDispose(info->outputFile);
      CFDateRef date=CFDateCreate(NULL,info->absoluteTime);
      [[RCSavedRecordingsModel sharedModel]
       insertRecordingWithAudioFile:((NSURL*)info->outputURL).path
       duration:info->duration date:(NSDate*)date customLabelBase:@"Call"];
      CFRelease(info->outputURL);
      CFRelease(date);
    }
    if(info->tokenValid[0]){notify_cancel(info->token[0]);}
    if(info->tokenValid[1]){notify_cancel(info->token[1]);}
    free(info);
  }
  F_DSPRoutingTelephony_destroy(self);
}
static int (*F_TelephonyStream_IOPerformOutput)(DSPRouting_Stream*,int,AudioBufferList*,AudioTimeStamp*);
static int f_TelephonyStream_IOPerformOutput(DSPRouting_Stream* self,int numFrames,AudioBufferList* data,AudioTimeStamp* tstamp){
  int status=F_TelephonyStream_IOPerformOutput(self,numFrames,data,tstamp);
  struct stream_info* info=(void*)self->name;
  if(info->name[0]=='+'){
    Float64 sampleRate=self->format.mSampleRate;
    ExtAudioFileRef outputFile=info->outputFile;
    if(!outputFile){
      const char* pstr=[[RCSavedRecordingsModel savedRecordingsDirectory]
       stringByAppendingPathComponent:@"call-XXXXXXXX.m4a"].fileSystemRepresentation;
      const size_t pstrlen=strlen(pstr);
      char* pbuf=memcpy(malloc(pstrlen+1),pstr,pstrlen+1);
      close(mkstemps(pbuf,strlen(strrchr(pstr,'.'))));
      CFURLRef outputURL=CFURLCreateFromFileSystemRepresentation(NULL,(UInt8*)pbuf,pstrlen,false);
      free(pbuf);
      if(ExtAudioFileCreateWithURL(outputURL,kAudioFileM4AType,&(AudioStreamBasicDescription){
       .mSampleRate=sampleRate,.mFormatID=kAudioFormatMPEG4AAC,.mFormatFlags=kMPEG4Object_AAC_Main,
       .mChannelsPerFrame=2},NULL,kAudioFileFlags_EraseFile,&outputFile)==noErr){
        AudioStreamBasicDescription inputFormat=self->format;
        inputFormat.mChannelsPerFrame=2;
        ExtAudioFileSetProperty(outputFile,kExtAudioFileProperty_ClientDataFormat,sizeof(inputFormat),&inputFormat);
        info->outputFile=outputFile;
        info->outputURL=outputURL;
        info->absoluteTime=CFAbsoluteTimeGetCurrent();
        info->duration=0;
      }
      else {
        outputFile=NULL;
        CFRelease(outputURL);
        info->name[0]='!';
      }
    }
    if(outputFile){
      struct {
        AudioBufferList buffer0;
        AudioBuffer buffer;
      } mixer;
      mixer.buffer0.mNumberBuffers=2;
      mixer.buffer0.mBuffers[0]=data->mBuffers[0];
      mixer.buffer=info->peerData->mBuffers[0];
      if(ExtAudioFileWrite(outputFile,numFrames,&mixer.buffer0)==noErr){info->duration+=numFrames/sampleRate;}
    }
  }
  return status;
}

static __attribute__((constructor)) void init() {
  _StreamMap=CFDictionaryCreateMutable(NULL,0,NULL,NULL);
  MSImageRef image=MSGetImageByName("/System/Library/Frameworks/AudioToolbox.framework/AudioToolbox");
  MSHookFunction(MSFindSymbol(image,"__ZN21DSP_Routing_Telephony10InitializeEv"),
   f_DSPRoutingTelephony_Initialize,(void*)&F_DSPRoutingTelephony_Initialize);
  MSHookFunction(MSFindSymbol(image,"__ZN21DSP_Routing_TelephonyD2Ev"),
   f_DSPRoutingTelephony_destroy,(void*)&F_DSPRoutingTelephony_destroy);
  MSHookFunction(MSFindSymbol(image,"__ZN21DSP_Routing_Telephony15TelephonyStream16IO_PerformOutputEiR15AudioBufferListRK14AudioTimeStamp"),
   f_TelephonyStream_IOPerformOutput,(void*)&F_TelephonyStream_IOPerformOutput);
}
