#include "common.h"

static const NSUInteger kRecordControlType=100;
static const CGFloat cImageScale=2;

%hook PHAudioCallControlArrangement
+(NSArray*)defaultControlTypes {
  return @[@[@0,@1,@2],@[@4,@5,@(kRecordControlType)]];
}
+(NSArray*)defaultMultipleCallControlTypes {
  return @[@[@0,@1,@2],@[@9,@8,@(kRecordControlType)]];
}
%end

%hook PHAudioCallControlsView
@interface PHAudioCallControlsView : UIView
-(void)setSelectedState:(BOOL)selected forControlType:(NSUInteger)type;
@end
-(void)assignControlType:(NSUInteger)type toButton:(UIButton*)button {
  %orig;
  if(type==kRecordControlType){
    static CGImageRef img0=NULL,img1;
    if(!img0){
      const CGRect rect=CGRectMake(0,0,50,50);
      const CGFloat lwidth=2,offA=lwidth/2,offB=offA+4;
      const CGRect rectA=CGRectInset(rect,offA,offA),rectB=CGRectInset(rect,offB,offB);
      CGColorSpaceRef cspace=CGColorSpaceCreateDeviceRGB();
      CGContextRef context=CGBitmapContextCreate(NULL,rect.size.width,rect.size.height,
       8,0,cspace,(CGBitmapInfo)kCGImageAlphaPremultipliedLast);
      CGColorSpaceRelease(cspace);
      CGContextSetLineWidth(context,lwidth);
      CGContextSetRGBFillColor(context,1,1,1,1);
      CGContextSetRGBStrokeColor(context,1,1,1,1);
      CGContextStrokeEllipseInRect(context,rectA);
      CGContextFillEllipseInRect(context,rectB);
      img0=CGBitmapContextCreateImage(context);
      CGContextClearRect(context,rect);
      CGContextSetRGBFillColor(context,0,0,0,1);
      CGContextSetRGBStrokeColor(context,0,0,0,1);
      CGContextStrokeEllipseInRect(context,rectA);
      CGContextFillEllipseInRect(context,rectB);
      img1=CGBitmapContextCreateImage(context);
      CGContextRelease(context);
    }
    [button setImage:[UIImage imageWithCGImage:img0
     scale:cImageScale orientation:UIImageOrientationUp] forState:UIControlStateNormal];
    [button setImage:[UIImage imageWithCGImage:img1
     scale:cImageScale orientation:UIImageOrientationUp] forState:UIControlStateSelected];
  }
}
%end

%hook PHAudioCallControlsViewController
@interface PHAudioCallControlsViewController : UIViewController
@property(assign) PHAudioCallControlsView* controlsView;
@end
-(NSString*)titleForControlType:(NSUInteger)type {
  return (type==kRecordControlType)?@"record":%orig;
}
-(BOOL)controlTypeIsSelected:(NSUInteger)type {
  return (type==kRecordControlType)?(BOOL)objc_getAssociatedObject(self,CALLRECORDER_ENABLE):%orig;
}
-(void)controlTypeTapped:(NSUInteger)type {
  if(type==kRecordControlType){
    BOOL enable=!objc_getAssociatedObject(self,CALLRECORDER_ENABLE);
    objc_setAssociatedObject(self,CALLRECORDER_ENABLE,(void*)enable,OBJC_ASSOCIATION_ASSIGN);
    notify_post(enable?CALLRECORDER_ENABLE:CALLRECORDER_DISABLE);
    [self.controlsView setSelectedState:enable forControlType:type];
  }
  else {%orig;}
}
%end
